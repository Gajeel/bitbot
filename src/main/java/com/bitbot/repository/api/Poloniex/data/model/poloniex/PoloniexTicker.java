package com.bitbot.repository.api.Poloniex.data.model.poloniex;

import com.google.gson.Gson;
import lombok.Getter;

import java.math.BigDecimal;

/**
 * @author David
 */
@Getter
public class PoloniexTicker {
    private final BigDecimal last;
    private final BigDecimal lowestAsk;
    private final BigDecimal highestBid;
    private final BigDecimal percentageChange;
    private final BigDecimal baseVolume;
    private final BigDecimal quoteVolume;

    public PoloniexTicker(BigDecimal last, BigDecimal lowestAsk, BigDecimal highestBid, BigDecimal percentageChange, BigDecimal baseVolume, BigDecimal quoteVolume) {
        this.last = last;
        this.lowestAsk = lowestAsk;
        this.highestBid = highestBid;
        this.percentageChange = percentageChange;
        this.baseVolume = baseVolume;
        this.quoteVolume = quoteVolume;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
