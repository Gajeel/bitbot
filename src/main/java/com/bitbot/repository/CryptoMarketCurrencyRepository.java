package com.bitbot.repository;

import com.bitbot.entity.CryptoMarketCurrency;

import java.util.Collection;

/**
 * Created by Kamil on 2017-06-16.
 */
public interface CryptoMarketCurrencyRepository {

    CryptoMarketCurrency findByCurrencyName(String currency);
    Collection<CryptoMarketCurrency> findAll();
}
