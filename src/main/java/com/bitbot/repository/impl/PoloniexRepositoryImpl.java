package com.bitbot.repository.impl;

import com.bitbot.entity.CryptoMarketCurrency;
import com.bitbot.repository.CryptoMarketCurrencyRepository;
import com.bitbot.repository.api.Poloniex.client.poloniex.PoloniexExchangeService;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Collections;

/**
 * Created by Kamil on 2017-06-16.
 */
@Repository
public class PoloniexRepositoryImpl implements CryptoMarketCurrencyRepository {

   final static PoloniexExchangeService service = new PoloniexExchangeService("","");

    @Override
    public CryptoMarketCurrency findByCurrencyName(String currency) {
        return new CryptoMarketCurrency(service.returnTicker(currency),currency);
    }

    @Override
    public Collection<CryptoMarketCurrency> findAll() {
        //List<PoloniexTicker> poloniexTickerList = service.returnAllMarkets()
        return Collections.EMPTY_LIST;
    }
}
