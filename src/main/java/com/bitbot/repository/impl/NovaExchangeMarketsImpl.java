package com.bitbot.repository.impl;

import com.bitbot.entity.CryptoMarketCurrency;
import com.bitbot.entity.markets.novaexchange.NovaMarket;
import com.bitbot.entity.markets.novaexchange.NovaExchange;
import com.bitbot.repository.CryptoMarketCurrencyRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Created by Kamil on 2017-06-16.
 */
@Repository
public class NovaExchangeMarketsImpl implements CryptoMarketCurrencyRepository {

    static final String novaExchangeAllMarkets = "https://novaexchange.com/remote/v2/markets/";
    static String novaExchangeOneMarket = "https://novaexchange.com/remote/v2/market/info/";

    @Override
    public CryptoMarketCurrency findByCurrencyName(@NotNull String currency) {
        RestTemplate restTemplate = new RestTemplate();
        NovaMarket novaMarket = restTemplate.getForObject(novaExchangeOneMarket+currency, NovaExchange.class).getMarkets()[0];
        return new CryptoMarketCurrency(novaMarket);

    }

    @Override
    public Collection<CryptoMarketCurrency> findAll() {
        RestTemplate restTemplate = new RestTemplate();
        NovaExchange novaExchange = restTemplate.getForObject(novaExchangeAllMarkets, NovaExchange.class);
        NovaMarket[] novMarkets = novaExchange.getMarkets();
        List<CryptoMarketCurrency> cryptoMarketCurrencies = new ArrayList<>(novMarkets.length);

        Arrays.stream(novMarkets).forEach(market -> cryptoMarketCurrencies.add(
                new CryptoMarketCurrency(market)
        ));
    return cryptoMarketCurrencies;
    }
}
