package com.bitbot.service;

import com.bitbot.entity.CryptoComparasion;
import com.bitbot.entity.CryptoMarketCurrency;
import com.bitbot.repository.impl.NovaExchangeMarketsImpl;
import com.bitbot.repository.impl.PoloniexRepositoryImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Kamil on 2017-06-16.
 */
@Service
public class CryptoCompareService {

    @Resource
    private NovaExchangeMarketsImpl novaExchangeMarketsImpl;

    @Resource
    private PoloniexRepositoryImpl poloniexRepositoryImpl;

    public CryptoComparasion getCryptoComparasionForCurrency(String currency) {

        CryptoMarketCurrency cryptoMarketCurrency1 = novaExchangeMarketsImpl.findByCurrencyName(currency);
        CryptoMarketCurrency cryptoMarketCurrency2 = poloniexRepositoryImpl.findByCurrencyName(currency);

        return new CryptoComparasion(cryptoMarketCurrency1, cryptoMarketCurrency2);
    }


}
