package com.bitbot.entity.markets.novaexchange;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;

/**
 * Created by Kamil on 2017-06-16.
 */
@Getter
@JsonIgnoreProperties(ignoreUnknown = true)
public class NovaMarket {

    private String basecurrency;

    private String volume24h;

    private String high24h;

    private String marketname;

    private String marketid;

    private String last_price;

    private String ask;

    private String change24h;

    private String low24h;

    private String bid;

    private String disabled;

    private String currency;
}
