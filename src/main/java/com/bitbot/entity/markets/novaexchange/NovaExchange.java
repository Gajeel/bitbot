package com.bitbot.entity.markets.novaexchange;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;

/**
 * Created by Kamil on 2017-06-16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class NovaExchange {

    private String message;

    private NovaMarket[] markets;

    private String status;
}
