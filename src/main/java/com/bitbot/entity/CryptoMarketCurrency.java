package com.bitbot.entity;

import com.bitbot.entity.markets.novaexchange.NovaMarket;
import com.bitbot.repository.api.Poloniex.data.model.poloniex.PoloniexTicker;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * Created by Kamil on 2017-06-16.
 */
@Getter
@ToString
public class CryptoMarketCurrency {

    private String name;
    private String currency;
    private BigDecimal ask;
    private BigDecimal bid;



    public CryptoMarketCurrency(String name, String currency, BigDecimal ask, BigDecimal bid) {
        this.name = name;
        this.currency = currency;
        this.ask = ask;
        this.bid = bid;
    }

    public CryptoMarketCurrency(){

    }

    public CryptoMarketCurrency(NovaMarket novaMarket){
        this.name = "NovaExchange";
        this.currency = novaMarket.getCurrency();
        this.ask = new BigDecimal(novaMarket.getAsk());
        this.bid = new BigDecimal(novaMarket.getBid());

    }

    public CryptoMarketCurrency(PoloniexTicker poloniexTicker, String currency){
        this.name="Poloniex";
        this.currency=currency;
        this.ask = poloniexTicker.getLowestAsk();
        this.bid = poloniexTicker.getHighestBid();
    }



}
