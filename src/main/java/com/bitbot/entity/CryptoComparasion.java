package com.bitbot.entity;

import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;

/**
 * Created by Kamil on 2017-06-16.
 */
@Getter
@ToString
public class CryptoComparasion {

    private CryptoMarketCurrency firstCryptoMarket;
    private CryptoMarketCurrency secondCryptoMarket;
    private String askSubstractBid;
    private float askSubstractBidPercent;
    private String bidSubstractAsk;
    private float bidSubstractAskPercent;

    public CryptoComparasion(CryptoMarketCurrency first, CryptoMarketCurrency second){
        this.firstCryptoMarket = first;
        this.secondCryptoMarket = second;
        this.askSubstractBid = firstCryptoMarket.getAsk().subtract(secondCryptoMarket.getBid()).toPlainString();
        this.askSubstractBidPercent = calculatePercentDiffrence(first.getAsk(), second.getBid());
        this.bidSubstractAsk = firstCryptoMarket.getBid().subtract(secondCryptoMarket.getAsk()).toPlainString();
        this.bidSubstractAskPercent = calculatePercentDiffrence(first.getBid(), second.getAsk());
    }

    private float calculatePercentDiffrence(BigDecimal first, BigDecimal second) {
        float firstFloat = Float.valueOf(first.toString());
        float secondFloat = Float.valueOf(second.toString());
        return (firstFloat * 100.00f) / secondFloat;

    }


}
