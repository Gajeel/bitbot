package com.bitbot.controller;

import com.bitbot.entity.CryptoComparasion;
import com.bitbot.service.CryptoCompareService;
import com.google.common.base.Splitter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.thymeleaf.util.ArrayUtils;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kamil on 2017-06-16.
 */
@Controller
public class HomeController {

    @Resource
    private CryptoCompareService cryptoCompareService;

    private String[] cryptoCurrencies = new String[]{"BTC_DGB", "BTC_BELA", "BTC_BLK", "BTC_BTCD", "BTC_BTM", "BTC_DASH", "BTC_DCR", "BTC_DOGE", "BTC_FLO", "BTC_NAV", "BTC_NOTE", "BTC_PINK", "BTC_POT", "BTC_PPC", "BTC_SYS", "BTC_VIA", "BTC_XBC", "BTC_XPM"};

    private Map<String, CryptoComparasion> comparasionMap = new HashMap<>();

    @RequestMapping("/home")
    public String home(Model model) {
        Arrays.stream(cryptoCurrencies).forEach(c -> comparasionMap.put(c, cryptoCompareService.getCryptoComparasionForCurrency(c)));
        model.addAttribute("avaiableCurrencies", Arrays.toString(cryptoCurrencies));
        model.addAttribute("comparasion", comparasionMap);
        return "home";
    }

    @RequestMapping("/home/{currency}")
    public String selectedCurrencies(@PathVariable("currency") String currency, Model model) throws Exception {
        Iterable<String> currencies = Splitter.on(",").omitEmptyStrings().trimResults().split(currency);

        for (String curr : currencies) {
            if (!ArrayUtils.contains(cryptoCurrencies, curr.toUpperCase())) {
                throw new Exception("Nie znaleziono waluty:" + curr + "\n" +
                        "Dostepne waluty to:" + Arrays.toString(cryptoCurrencies));
            }
            comparasionMap.put(curr, cryptoCompareService.getCryptoComparasionForCurrency(curr.toUpperCase()));

        }
        model.addAttribute("avaiableCurrencies", Arrays.toString(cryptoCurrencies));
        model.addAttribute("comparasion", comparasionMap);
        return "home";
    }

}
